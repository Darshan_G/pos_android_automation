def Chef_login(driver):
    E_MAIL_ID = driver.find_element_by_id("com.digicollect.pos:id/input_email")
    E_MAIL_ID.send_keys("chef@testhavana.com")
    Password = driver.find_element_by_id("com.digicollect.pos:id/input_password")
    Password.send_keys("testtest")
    log_in = driver.find_element_by_id("com.digicollect.pos:id/btn_login")
    log_in.click()

    search_button = driver.find_element_by_accessibility_id("Search Orders")
    search_button.click()
    search_text = driver.find_element_by_id("com.digicollect.pos:id/search_src_text")
    search_text.send_keys("Darshan")

    select_order = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
        ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout"
        "/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget"
        ".FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]")
    select_order.click()
    Prepare_all = driver.find_element_by_id("com.digicollect.pos:id/switch_prepered")
    Prepare_all.click()
    Prepared = driver.find_element_by_accessibility_id("PREPARED")
    Prepared.click()

    Home = driver.find_element_by_accessibility_id("Open")
    Home.click()
    menu = driver.find_element_by_accessibility_id("Open")
    menu.click()
    Log_out = driver.find_element_by_id("com.digicollect.pos:id/logout")
    Log_out.click()

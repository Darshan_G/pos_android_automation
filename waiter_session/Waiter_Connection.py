from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
import pdb

from chef_session.Chef_Log_in import Chef_login
from manager_session.CheckOut_Order_Summary import Check_Out
from manager_session.Dashboard import Table_Avoilalable
from manager_session.Dine_In import dine_in_SingleTable
from manager_session.Payment_Method_Screen import Cash_Pay
from manager_session.Post_payed_order import Post_Pay
from manager_session.Take_Away_Order import take_away
from manager_session.Your_Order_Summary_Screen import Pay_Close
from manager_session.select_course_and_product import select_course_product
from waiter_session.Manage_Table_Waiter import ManageOrder_waiter
from waiter_session.Waiter_Log_in import waiter_log_in
from waiter_session.Waiter_Log_out import waiter_Logout

desired_cap = {
    "deviceName": "10.1_WXGA_Tablet_API_27",
    "platformName": "android",
    "appPackage": "com.digicollect.pos",
    "appActivity": ".activity.SplashActivity",
    "noReset": "true"
}

driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
driver.implicitly_wait(30)

#  Log-In page

waiter_log_in(driver)

Table_Avoilalable(driver)

take_away(driver)
print("Take away order with Pre pay is done by Waiter Log-In")

# dine_in_SingleTable(driver)
# print("Dine-in Place Order is started table selected")

select_course_product(driver)
print("Course, Product and Addon's selected successfully")

                                         # Check out screen in place order flow #

Check_Out(driver)
print("Order additional charge and Discount is done")

                                               # Payment types selection #

# Fast_Pay_Method(driver)
# print("Fast Pay selection is done")

# Pre_Pay(driver)
# print("Pre pied order is selected ")

Post_Pay(driver)
print("Post Pay Order is Selected")

                        # Payment Method screen and types of payment can done here #

# Cash_Pay(driver)
# print("Cash Payment is Successfully Completed")

# card_pay(driver)
# print("Card Payment is Successfully Completed")

# other_payment(driver)
# print("Other Payment is successfully Completed")

# split_bill(driver)
# print("Split bill payment of cash, Card and Other payment Successfully Completed")


ManageOrder_waiter(driver)
Pay_Close(driver)

# Payment Method screen and types of payment can done here #

Cash_Pay(driver)
print("Cash Payment is Successfully Completed")

# card_pay(driver)
# print("Card Payment is Successfully Completed")

# other_payment(driver)
# print("Other Payment is successfully Completed")

# split_bill(driver)
# print("Split bill payment of cash, Card and Other payment Successfully Completed")


# Cash_Pay(driver)
print("done")




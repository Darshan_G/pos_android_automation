import pdb

from chef_session.Chef_Log_in import Chef_login
from manager_session.Your_Order_Summary_Screen import Pay_Close
from waiter_session.Waiter_Log_in import waiter_log_in
from waiter_session.Waiter_Log_out import waiter_Logout


def ManageOrder_waiter(driver):
    Mange_order = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android."
        "widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget"
        ".RelativeLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout")
    Mange_order.click()

    driver.implicitly_wait(40)

    Order_Search = driver.find_element_by_accessibility_id("Search Orders")
    Order_Search.click()
    search_text = driver.find_element_by_id("com.digicollect.pos:id/search_src_text")
    search_text.send_keys("Darshan")
    select = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/"
        "android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.recyclerview.widget"
        ".RecyclerView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]")
    select.click()

    fire = driver.find_element_by_id("com.digicollect.pos:id/switch_fired")
    fire.click()

    # retry = driver.find_element_by_id("com.digicollect.pos:id/refresh")
    # retry.click()
    driver.implicitly_wait(40)

    try:
        Without_print = driver.find_element_by_id("com.digicollect.pos:id/continue_printing")
        Without_print.click()
    except:
        pass

    waiter_Logout(driver)

    Chef_login(driver)

    waiter_log_in(driver)

    Mange_order = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android."
        "widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget"
        ".RelativeLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout")
    Mange_order.click()
    driver.implicitly_wait(30)
    search = driver.find_element_by_accessibility_id("Search Orders")
    search.click()
    search_text = driver.find_element_by_id("com.digicollect.pos:id/search_src_text")
    search_text.send_keys("Darshan")

    select_order = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager"
        "/android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.recyclerview.widget"
        ".RecyclerView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout")
    select_order.click()
    deliver = driver.find_element_by_id("com.digicollect.pos:id/switch_prepered_delivered")
    deliver.click()

    Pay_and_close_order = driver.find_element_by_id("com.digicollect.pos:id/btn_pay_n_close_order")
    Pay_and_close_order.click()

    # Cash_Pay(driver)

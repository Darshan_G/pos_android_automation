def waiter_log_in(driver):
    email = driver.find_element_by_id("com.digicollect.pos:id/input_email")
    email.send_keys("waiter@testhavana.com")
    driver.press_keycode(61)

    # Password

    password = driver.find_element_by_id("com.digicollect.pos:id/input_password")
    password.send_keys("testtest")

    driver.find_element_by_id("com.digicollect.pos:id/btn_login").click()
    driver.implicitly_wait(30)

    Kitchen_Printner = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout"
                                                    "/android.widget.FrameLayout/android.widget.LinearLayout/android."
                                                    "widget.FrameLayout/android.widget.RelativeLayout/android.widget."
                                                    "ScrollView/android.widget.RelativeLayout/android.widget."
                                                    "RelativeLayout[2]/androidx.recyclerview.widget.RecyclerView/android"
                                                    ".widget.RelativeLayout[1]/android.widget.RelativeLayout/android"
                                                    ".widget.CheckBox")
    Kitchen_Printner.click()

    Bar_Printer = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android"
        ".widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout"
        "/android.widget.RelativeLayout/android.widget.ScrollView/android.widget."
        "RelativeLayout/android.widget.RelativeLayout[3]/androidx.recyclerview."
        "widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget."
        "RelativeLayout/android.widget.CheckBox")
    Bar_Printer.click()

    RF_ID = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android."
        "widget.RelativeLayout/android.widget.ScrollView/android.widget.RelativeLayout"
        "/android.widget.RelativeLayout[4]/androidx.recyclerview.widget.RecyclerView/"
        "android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android"
        ".widget.CheckBox")
    RF_ID.click()

    Proceed = driver.find_element_by_id("com.digicollect.pos:id/btn_proceed")
    Proceed.click()
    driver.implicitly_wait(30)
    print("Log-In is Done")
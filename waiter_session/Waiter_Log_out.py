import pdb


def waiter_Logout(driver):
    home = driver.find_element_by_accessibility_id("Discard")
    home.click()
    pdb.set_trace()
    driver.implicitly_wait(30)
    menu = driver.find_element_by_accessibility_id("Open")
    menu.click()
    pdb.set_trace()
    el3 = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
        ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/android"
        ".widget.FrameLayout/android.widget.RelativeLayout")
    el3.click()


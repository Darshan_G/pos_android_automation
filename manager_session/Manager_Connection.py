from appium import webdriver
from manager_session.CheckOut_Order_Summary import Check_Out
from manager_session.Dashboard import Table_Avoilalable
from manager_session.Dine_In import dine_in_SingleTable
from manager_session.Fast_Pay import Fast_Pay_Method, Product_screen_fast_pay, Addon_screen_fast_pay
from manager_session.Fast_checkout import Product_screen_fast_checkout
from manager_session.Manage_Order_Field import Manage_Order_by_CardView
from manager_session.Mnager_login import manager_log_in
from manager_session.Payment_Method_Screen import Cash_Pay, card_pay, other_payment
from manager_session.Post_payed_order import Post_Pay
from manager_session.Pre_Payed import Pre_Pay
from manager_session.Split_bill_payment_method import split_bill
from manager_session.Take_Away_Order import take_away
from manager_session.dine_in_multiple import dine_in_multi_order
from manager_session.select_course_and_product import select_course_product
from manager_session.select_multiple_course_and_product import select_multi_course_product
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select


desired_cap = {
    "deviceName": "Galaxy Tab S2",
    "platformName": "android",
    "appPackage": "com.digicollect.pos",
    "appActivity": ".activity.SplashActivity",
    "noReset": "true"
}

driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
driver.implicitly_wait(30)

# Manager Log In creadentials and printer selection screen #
try:
    manager_log_in(driver)
    print("Log-In is Done")

# Dash board screen to Select place order or Manage order #
except:
    pass

Table_Avoilalable(driver)

# Take-Away order selected and continue #

# take_away(driver)
# print("Take away table is selected")

dine_in_SingleTable(driver)
print("Dine-in Place Order is started table selected")

# dine_in_multi_order(driver)

                          # Selecting the Course, Category, Sub Category and product and addon's #
    # Select single course multi product and single addon is selecting #

# select_course_product(driver)
# print("Course, Product and Addon's selected successfully")

    # Select 2 course, 2 course and multi product with single addon is selecting #

select_multi_course_product(driver)
print("Two course 2 product and multiple products are selected")

# From Product Screen Fast Pay and Fast check out #

# Product_screen_fast_pay(driver)
# print("From Product screen Fast pay")

Product_screen_fast_checkout(driver)
print("product screen Fast checkout screen selected")

# From Check out screen in place order flow #

# Check_Out(driver)
# print("Order additional charge and Discount is done")

# Payment types selection #

# Fast_Pay_Method(driver)
# print("Fast Pay selection is done")

# Pre_Pay(driver)
# print("Pre pied order is selected ")

# Post_Pay(driver)
# print("Post Pay Order is Selected")

# Payment Method screen and types of payment can done here #

# Cash_Pay(driver)
# print("Cash Payment is Successfully Completed")

# card_pay(driver)
# print("Card Payment is Successfully Completed")

# other_payment(driver)
# print("Other Payment is successfully Completed")

# split_bill(driver)
# print("Split bill payment of cash, Card and Other payment Successfully Completed")

# Manage order card view #

Manage_Order_by_CardView(driver)

Cash_Pay(driver)
print("Cash Payment is Successfully Completed")

# card_pay(driver)
# print("Card Payment is Successfully Completed")

# other_payment(driver)
# print("Other Payment is successfully Completed")

# split_bill(driver)
# print("Split bill payment of cash, Card and Other payment Successfully Completed")


# dine_in_multi_order(driver)
# print("Dine-in with multiple table, multiple product and multiple addon done")
# Table_Avoilalable(driver)

# Table_Avoilalable(driver)
# manage_order(driver)

print("Manage Order is Done")


from appium.webdriver.common.touch_action import TouchAction
from manager_session.Manage_Table import ManageOrder
from manager_session.Your_Order_Summary_Screen import Pay_Close


def manage_order(driver):
    TouchAction(driver).press(x=770, y=1870).move_to(x=766, y=484).release().perform()

    TouchAction(driver).press(x=763, y=1870).move_to(x=756, y=456).release().perform()

    TouchAction(driver).press(x=770, y=1860).move_to(x=759, y=456).release().perform()

    TouchAction(driver).press(x=766, y=1839).move_to(x=770, y=522).release().perform()

    TouchAction(driver).press(x=766, y=1891).move_to(x=759, y=498).release().perform()

    TouchAction(driver).press(x=770, y=1881).move_to(x=763, y=498).release().perform()

    TouchAction(driver).press(x=770, y=1682).move_to(x=773, y=474).release().perform()

    number = 18
    select_table = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/"
                                                "android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
                                                ".FrameLayout/android.widget.RelativeLayout/androidx.drawerlayout"
                                                ".widget.DrawerLayout/android.widget.RelativeLayout/android.widget."
                                                "FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager"
                                                "/android.widget.RelativeLayout/android.view.ViewGroup/androidx."
                                                "recyclerview.widget.RecyclerView/android.widget"
                                                ".FrameLayout[" + str(number) + "]"
                                                                                "/android.widget.LinearLayout/android.widget.TextView[1]")
    select_table.click()
    ManageOrder(driver)
    Pay_Close(driver)

    # fast_pay(driver)

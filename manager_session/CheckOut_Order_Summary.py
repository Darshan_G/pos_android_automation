import pdb

from manager_session.Fast_Pay import Fast_Pay_Method
from manager_session.Post_payed_order import Post_Pay
from manager_session.Pre_Payed import Pre_Pay
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


def Check_Out(driver):
    # action = ActionChains(driver)
    # action.send_keys(Keys.ARROW_DOWN).perform()

    checkout = driver.find_element_by_id("com.digicollect.pos:id/cart_view_btn")
    checkout.click()

    scroll_view = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    scroll_view.click()

    TouchAction(driver).press(x=130, y=761).move_to(x=178, y=100).release().perform()
    TouchAction(driver).press(x=130, y=761).move_to(x=178, y=100).release().perform()

    Item_discount = driver.find_element_by_id("com.digicollect.pos:id/img_view_show_alert")
    Item_discount.click()
    Add_discount = driver.find_element_by_id("com.digicollect.pos:id/app_btn_check_out_add_discount")
    Add_discount.click()
    Value_in_cash = driver.find_element_by_id("com.digicollect.pos:id/edt_text_alert_discount")
    Value_in_cash.send_keys("10")
    Add = driver.find_element_by_id("android:id/button1")
    Add.click()
    driver.implicitly_wait(50)

    product_total_amount = driver.find_element_by_id("com.digicollect.pos:id/txt_view_product_total_amount")
    total_amount = product_total_amount.get_attribute("text")
    print(total_amount)

    # scroll_view = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    # scroll_view.click()
    # TouchAction(driver).press(x=619, y=1957).move_to(x=595, y=372).release().perform()

    # pdb.set_trace()

    scroll_view = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    scroll_view.click()

    TouchAction(driver).press(x=524, y=623).move_to(x=566, y=140).release().perform()

    scroll_view = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    scroll_view.click()

    TouchAction(driver).press(x=619, y=1957).move_to(x=595, y=372).release().perform()

    qty_tab_to_down = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/"
        "android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android"
        ".widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout/android.widget.TableLayout/android.widget.TableRow/androidx.recyclerview.widget"
        ".RecyclerView/android.view.ViewGroup[3]/android.widget.LinearLayout/android.view.ViewGroup/android"
        ".widget.LinearLayout/android.widget.EditText")
    qty_tab_to_down.click()

    for tab in range(15):
        # pdb.set_trace()
        driver.press_keycode(61)
    driver.hide_keyboard()

    driver.find_element_by_id("com.digicollect.pos:id/scrollView")

    TouchAction(driver).press(x=623, y=1775).move_to(x=607, y=111).release().perform()

    driver.implicitly_wait(30)

    Order_additional_charge_edit = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/"
        "android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android."
        "widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout/android.widget.TableLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[6]"
        "/android.widget.LinearLayout[2]/android.widget.TextView")
    Order_additional_charge_edit.click()
    Order_additional_value = driver.find_element_by_id("com.digicollect.pos:id/edt_text_alert_charge")
    Order_additional_value.send_keys("1000")
    Add_additional_value = driver.find_element_by_id("android:id/button1")
    Add_additional_value.click()
    driver.implicitly_wait(50)

    Order_discount_edit = driver.find_element_by_id("com.digicollect.pos:id/txt_view_order_discount")
    Order_discount_edit.click()
    Select_cash_mode = driver.find_element_by_id("com.digicollect.pos:id/rd_btn_discount_cash")
    Select_cash_mode.click()
    Order_discount_value = driver.find_element_by_id("com.digicollect.pos:id/edt_text_alert_discount")
    Order_discount_value.send_keys("500")
    Add_discount_value = driver.find_element_by_id("android:id/button1")
    Add_discount_value.click()
    driver.implicitly_wait(50)

    scroll_view = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    scroll_view.click()
    TouchAction(driver).press(x=648, y=1800).move_to(x=690, y=326).release().perform()



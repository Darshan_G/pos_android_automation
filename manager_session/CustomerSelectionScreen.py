def Select_Customer(driver):
    select_customer = driver.find_element_by_id("com.digicollect.pos:id/txt_search_view_customer")
    select_customer.click()
    search = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android"
                                          ".widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout"
                                          "/android.widget.RelativeLayout/androidx.drawerlayout.widget.DrawerLayout"
                                          "/android.widget.RelativeLayout/android.widget.FrameLayout/android.view."
                                          "ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.RelativeLayout"
                                          "/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget."
                                          "FrameLayout[1]/androidx.appcompat.widget.LinearLayoutCompat/android.widget"
                                          ".LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout"
                                          "/android.widget.EditText")
    search.click()
    search.send_keys("Darshan")
    select = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
        ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager"
        "/android.widget.RelativeLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android"
        ".widget.FrameLayout[2]/android.widget.LinearLayout/androidx.recyclerview.widget"
        ".RecyclerView/android.widget.RelativeLayout[2]")

    select.click()
    proceed = driver.find_element_by_id("com.digicollect.pos:id/btn_proceed")
    proceed.click()
    print("Customer Selection is Done")
import pdb

from manager_session.Manage_Order_By_Table import ManageOrder


def Manage_Order_by_CardView(driver):
    Manage_order_dashboard = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android."
        "widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget"
        ".RelativeLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout")
    Manage_order_dashboard.click()

    pdb.set_trace()
    search_Order = driver.find_element_by_accessibility_id("Search Orders")
    search_Order.click()
    text = driver.find_element_by_id("com.digicollect.pos:id/search_src_text")
    text.click()
    text.send_keys("Darshan")
    Select_order = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager"
        "/android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.recyclerview.widget"
        ".RecyclerView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout")
    Select_order.click()

    ManageOrder(driver)
    # Pay_Close(driver)

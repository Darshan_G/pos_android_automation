from appium.webdriver.common.touch_action import TouchAction
from manager_session.Addon_selection_screen import select_addon
from manager_session.CheckOut_Order_Summary import Check_Out
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


def select_course_product(driver):
    select_course = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
        ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android"
        ".widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[3]"
        "/android.widget.RelativeLayout/android.widget.TextView")
    select_course.click()
    key = 7
    select_category = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout"
        "/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx"
        ".drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android"
        ".widget.RelativeLayout/android.widget.ScrollView/android.widget.RelativeLayout/androidx.recyclerview"
        ".widget.RecyclerView[2]/android.widget.RelativeLayout[" + str(key) + "]/android.widget.FrameLayout")
    select_category.click()

    select_subcat = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
        ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
        ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android"
        ".widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[3]/android.widget."
        "RelativeLayout/android.widget.FrameLayout")
    select_subcat.click()

    # driver.press_keycode(274)

    # select_product_type = driver.find_element_by_xpath(
    #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
    #     "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
    #     "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
    #     "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView"
    #     "/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[4]/android"
    #     ".widget.RelativeLayout/android.widget.FrameLayout")
    # select_product_type.click()

    # pager = driver.find_elements_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android"
    # ".widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout"
    # "/android.widget.RelativeLayout/android.support.v4.widget.DrawerLayout/android"
    # ".widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android"
    #  ".support.v4.view.ViewPager")
    # pager.click(TouchAction(driver).press(x=617, y=2148).move_to(x=696, y=1104).release().perform())
    # TouchAction(driver)   .press(x=599, y=1773)   .move_to(x=623, y=630)   .release()   .perform()

    value = 1
    for i in range(2):
        select_product = driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
            ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
            "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
            "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android"
            ".widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[4]/android.widget."
            "FrameLayout[" + str(value) + "]/android.widget.FrameLayout/android.widget.LinearLayout/"
                                          "android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.EditText")
        select_product.click()

        select_addon(driver)

        value += 1

    # for i in range(4):
    #     select_subcat = driver.find_element_by_xpath(
    #         "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
    #         ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
    #         ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
    #         "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android"
    #         ".widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[3]/android.widget."
    #         "RelativeLayout/android.widget.FrameLayout")
    #     select_subcat.click()
    #     actions = ActionChains(driver)
    #     actions.send_keys(Keys.ARROW_DOWN).perform()
    # # actions.move_to_element(text_view)
    # # actions.click_and_hold().build()
    # # actions.perform()
    #
    # text_view = driver.find_element_by_xpath(
    #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
    #     "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
    #     "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/"
    #     "android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android."
    #     "widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[8]"
    #     "/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android."
    #     "widget.RelativeLayout/android.widget.EditText")
    #
    # actions = ActionChains(driver)
    # actions.send_keys(Keys.ARROW_DOWN).perform()
    # # actions.move_to_element(text_view)
    # # actions.click_and_hold().build()
    # # actions.perform()
    #
    # text_view.click()

    # checkout = driver.find_element_by_id("com.digicollect.pos:id/cart_view_btn")
    # checkout.click()

    # fast_pay(driver)

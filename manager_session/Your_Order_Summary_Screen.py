from appium.webdriver.common.touch_action import TouchAction
from manager_session.Payment_Method_Screen import Cash_Pay


def Pay_Close(driver):
    scroll_view = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    scroll_view.click()
    TouchAction(driver).press(x=673, y=896).move_to(x=698, y=62).release().perform()

    scroll_view = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    scroll_view.click()
    TouchAction(driver).press(x=751, y=1965).move_to(x=793, y=54).release().perform()

    try:
        pay_and_close = driver.find_element_by_id("com.digicollect.pos:id/btn_pay")
        pay_and_close.click()
    except:
        close_order = driver.find_element_by_id("com.digicollect.pos:id/btn_close")
        close_order.click()
        confirm_close = driver.find_element_by_id("android:id/button1")
        confirm_close.click()

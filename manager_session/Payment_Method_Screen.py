import pdb

from appium.webdriver.common.touch_action import TouchAction


def Cash_Pay(driver):
    # pay = driver.find_element_by_id("com.digicollect.pos:id/btn_fast_pay")
    # pay.click()

    driver.implicitly_wait(50)
    pdb.set_trace()

    cash_tender = driver.find_element_by_id("com.digicollect.pos:id/edt_txt_total_cash_tendered")
    cash_tender.send_keys("2000")

    submit = driver.find_element_by_id("com.digicollect.pos:id/btn_submit")
    submit.click()

    yes = driver.find_element_by_id("android:id/button1")
    yes.click()

    driver.implicitly_wait(30)

    for i in range(2):
        try:
            continue_without_print = driver.find_element_by_id("com.digicollect.pos:id/continue_printing")
            continue_without_print.click()
        except:
            pass


def card_pay(driver):
    radio_card = driver.find_element_by_id("com.digicollect.pos:id/rd_btn_primary_customer_card")
    radio_card.click()
    issuing_bank = driver.find_element_by_id("com.digicollect.pos:id/edt_issuing_bank")
    issuing_bank.send_keys("SBM Mysore")
    radio_card_type = driver.find_element_by_id("com.digicollect.pos:id/rd_btn_credit_card")
    radio_card_type.click()
    card_provider_click = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/"
        "android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[2]"
        "/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout"
        "/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]"
        "/android.widget.Spinner/android.widget.TextView")
    card_provider_click.click()
    card_provider = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget."
        "FrameLayout/android.widget.ListView/android.widget.TextView[11]")
    card_provider.click()
    name_on_card = driver.find_element_by_id("com.digicollect.pos:id/edt_name_on_card")
    name_on_card.send_keys("Darshan")

    transaction_id = driver.find_element_by_id("com.digicollect.pos:id/edt_transcition_id")
    transaction_id.send_keys("1234")

    card_number = driver.find_element_by_id("com.digicollect.pos:id/edt_card_number")
    card_number.click()

    TouchAction(driver).tap(x=512, y=1508).perform()

    TouchAction(driver).tap(x=672, y=1508).perform()

    TouchAction(driver).tap(x=857, y=1473).perform()

    TouchAction(driver).tap(x=467, y=1613).perform()

    TouchAction(driver).tap(x=669, y=1627).perform()

    TouchAction(driver).tap(x=512, y=1508).perform()

    driver.hide_keyboard()
    TouchAction(driver).press(x=620, y=1888).move_to(x=676, y=341).release().perform()

    submit = driver.find_element_by_id("com.digicollect.pos:id/btn_submit")
    submit.click()

    yes = driver.find_element_by_id("android:id/button1")
    yes.click()

    driver.implicitly_wait(30)

    for i in range(2):
        try:
            continue_without_print = driver.find_element_by_id("android:id/button2")
            continue_without_print.click()
        except:
            pass


def other_payment(driver):
    radio_other = driver.find_element_by_id("com.digicollect.pos:id/rd_btn_primary_customer_others")
    radio_other.click()
    other_pay = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/"
        "android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[2]/"
        "android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android"
        ".widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android"
        ".widget.Spinner/android.widget.TextView")
    other_pay.click()
    select_other_pay_type = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout"
        "/android.widget.ListView/android.widget.TextView[4]")
    select_other_pay_type.click()
    reference_num = driver.find_element_by_id("com.digicollect.pos:id/mEdt_refernce")
    reference_num.send_keys("123045")
    submit = driver.find_element_by_id("com.digicollect.pos:id/btn_submit")
    submit.click()

    yes = driver.find_element_by_id("android:id/button1")
    yes.click()

    driver.implicitly_wait(30)

    try:
        continue_without_print = driver.find_element_by_id("com.digicollect.pos:id/continue_printing")
        continue_without_print.click()
    except:
        pass

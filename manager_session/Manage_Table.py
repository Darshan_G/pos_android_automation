from appium.webdriver.common.touch_action import TouchAction
from manager_session.Your_Order_Summary_Screen import Pay_Close


def ManageOrder(driver):
    Fire_Order = driver.find_element_by_id("com.digicollect.pos:id/switch_fired")
    Fire_Order.click()
    try:
        Continue_Without_print = driver.find_element_by_id("com.digicollect.pos:id/continue_printing")
        Continue_Without_print.click()
    except:
        pass

    Prepare_Order = driver.find_element_by_id("com.digicollect.pos:id/switch_prepered")
    Prepare_Order.click()
    Deliver_Order = driver.find_element_by_id("com.digicollect.pos:id/switch_prepered_delivered")
    Deliver_Order.click()

    try:
        PayClose = driver.find_element_by_id("com.digicollect.pos:id/btn_pay_n_close_order")
        PayClose.click()

    except:
        Close_order = driver.find_element_by_id("com.digicollect.pos:id/btn_pay_n_close_order")
        Close_order.click()

    Pay_Close(driver)

    # Scroll = driver.find_element_by_id("com.digicollect.pos:id/scrollView")
    # Scroll.click()
    # TouchAction(driver).press(x=367, y=599).move_to(x=396, y=50).release().perform()
    #
    # TouchAction(driver).press(x=545, y=1924).move_to(x=500, y=95).release().perform()
    #
    # close = driver.find_element_by_id("com.digicollect.pos:id/btn_close")
    # close.click()
    # yes = driver.find_element_by_id("android:id/button1")
    # yes.click()

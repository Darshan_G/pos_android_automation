import pdb

from appium.webdriver.common.touch_action import TouchAction


def split_bill(driver):
    split = driver.find_element_by_id("com.digicollect.pos:id/btn_split_bill")
    split.click()

    TouchAction(driver).press(x=657, y=1800).move_to(x=623, y=58).release().perform()

    add_payment = driver.find_element_by_id("com.digicollect.pos:id/txt_view_add_payment")
    add_payment.click()

    TouchAction(driver).press(x=681, y=888).move_to(x=632, y=2003).release().perform()

    split_equally = driver.find_element_by_id("com.digicollect.pos:id/btn_split_equally")
    split_equally.click()

    # Cash Pay method #

    Cash_to_pay = driver.find_element_by_id("com.digicollect.pos:id/edit_txt_primary_customer_amount")
    cash_to = Cash_to_pay.get_attribute("text")

    cash_tender = driver.find_element_by_id("com.digicollect.pos:id/edt_txt_total_cash_tendered")
    cash_tender.send_keys(cash_to)

    # Card Payment Method #

    TouchAction(driver).press(x=512, y=1846).move_to(x=599, y=606).release().perform()

    TouchAction(driver).press(x=1066, y=801).move_to(x=1073, y=986).release().perform()

    card_radio = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android"
        ".widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget"
        ".LinearLayout[1]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "RelativeLayout[2]/android.widget.RadioGroup/android.widget.RadioButton[2]")
    card_radio.click()
    issuing_bank = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/"
        "android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/"
        "android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout"
        "/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout"
        "/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget."
        "FrameLayout/android.widget.EditText")
    issuing_bank.send_keys("SBM")
    card_type_credit_radio = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android"
        ".widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget."
        "RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.RadioGroup/android.widget.RadioButton[1]")
    card_type_credit_radio.click()
    card_provider_click = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/"
        "android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android."
        "widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget"
        ".LinearLayout[1]/android.widget.Spinner/android.widget.TextView")
    card_provider_click.click()
    card_provider = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout"
        "/android.widget.ListView/android.widget.TextView[3]")
    card_provider.click()
    name_on_card = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android"
        ".widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget"
        ".LinearLayout[2]/android.widget.FrameLayout/android.widget.EditText")
    name_on_card.send_keys("SBM")

    card_number = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android"
        ".widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android.widget."
        "ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout"
        "/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android."
        "widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget."
        "LinearLayout[1]/android.widget.FrameLayout/android.widget.EditText")
    card_number.click()
    TouchAction(driver).press(x=1090, y=829).move_to(x=1090, y=568).release().perform()

    TouchAction(driver).tap(x=644, y=1494).perform()
    TouchAction(driver).tap(x=658, y=1620).perform()
    TouchAction(driver).tap(x=864, y=1627).perform()
    TouchAction(driver).tap(x=463, y=1623).perform()
    transaction_id = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/"
        "android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android."
        "widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget."
        "RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout[3]/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.EditText")
    transaction_id.send_keys("SBM")
    driver.hide_keyboard()
    TouchAction(driver).press(x=812, y=1741).move_to(x=812, y=606).release().perform()

    # Other Payment Method Starts #

    Other_pay_radio = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android."
        "widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[1]/android.widget"
        ".LinearLayout[2]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget."
        "RelativeLayout[2]/android.widget.RadioGroup/android.widget.RadioButton[3]")
    Other_pay_radio.click()

    drop_down_click = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/"
        "android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android."
        "widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[1]/android.widget."
        "LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget."
        "RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout"
        "/android.widget.LinearLayout[1]/android.widget.Spinner/android.widget.TextView")
    drop_down_click.click()
    drop_down_value = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout"
        "/android.widget.ListView/android.widget.TextView[9]")
    drop_down_value.click()
    reference_id = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
        "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android."
        "widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[1]/android.widget."
        "LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout"
        "/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget."
        "LinearLayout[2]/android.widget.FrameLayout/android.widget.EditText")
    reference_id.send_keys("SBM")

        # Remaining amount Check to make balance #

    remaining_value = driver.find_element_by_id("com.digicollect.pos:id/txt_remaining_value")

    # Getting that remaining value using get attribute as float point #

    remaining = float(remaining_value.get_attribute("text"))
    print(remaining)

    # If Condition to check the remaining and current pay #

    if remaining != 0:

        cash = driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget."
            "FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
            "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/"
            "android.widget.FrameLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android."
            "widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout[1]/android.widget"
            ".LinearLayout/android.widget.EditText")

        # Getting Current Payable value after split equally #

        current_pay = float(cash.get_attribute("text"))
        print(current_pay)

        # Balancing the total by adding remaining amount and current pay amount to make present pay and make equal or Zero #

        present = current_pay + remaining
        print(present)

        # converging the float value to string to make comfortable for code #

        present_pay = str(present)
        print(present_pay)

        cash.send_keys(present_pay)

    TouchAction(driver).press(x=1111, y=1769).move_to(x=1066, y=380).release().perform()

    submit = driver.find_element_by_id("com.digicollect.pos:id/btn_submit")
    submit.click()

    yes = driver.find_element_by_id("android:id/button1")
    yes.click()

    driver.implicitly_wait(30)

    try:
        continue_without_print = driver.find_element_by_id("android:id/button2")
        continue_without_print.click()
    except:
        pass

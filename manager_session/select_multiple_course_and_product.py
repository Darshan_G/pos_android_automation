from manager_session.Addon_selection_screen import select_addon


def select_multi_course_product(driver):
    key = 6
    for course in range(2):
        select_course = driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
            ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
            ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
            "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android"
            ".widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[1]/android.widget.FrameLayout[3]"
            "/android.widget.RelativeLayout/android.widget.TextView")
        select_course.click()

        select_category = driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout"
            "/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx"
            ".drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android"
            ".widget.RelativeLayout/android.widget.ScrollView/android.widget.RelativeLayout/androidx.recyclerview"
            ".widget.RecyclerView[2]/android.widget.RelativeLayout[" + str(key) + "]/android.widget.FrameLayout")
        select_category.click()

        select_subcat = driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
            ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
            ".RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
            "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android"
            ".widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[3]/android.widget."
            "RelativeLayout/android.widget.FrameLayout")
        select_subcat.click()

        key += 1

        value = 1
        for product in range(2):
            select_product = driver.find_element_by_xpath(
                "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
                ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget."
                "RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout"
                "/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android"
                ".widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView[4]/android.widget."
                "FrameLayout[" + str(value) + "]/android.widget.FrameLayout/android.widget.LinearLayout/"
                                              "android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.EditText")
            select_product.click()

            value += 1

            select_addon(driver, course, product)





from appium.webdriver.common.touch_action import TouchAction
from manager_session.Fast_Pay import Addon_screen_fast_pay
from manager_session.Fast_checkout import Addon_screen_fast_checkout


def select_addon(driver, course, product):
    #  Addon Selection Screen  #

    select_addons = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android"
        ".widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout"
        "/android.widget.RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/"
        "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/"
        "android.widget.RelativeLayout/android.widget.ScrollView/android.widget."
        "LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]"
        "/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.FrameLayout")
    select_addons.click()

    scroll = driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android"
        ".widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout"
        "/android.widget.RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/"
        "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget."
        "RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/"
        "android.widget.RelativeLayout/android.widget.ScrollView")
    scroll.click()

    TouchAction(driver).press(x=485, y=865).move_to(x=470, y=230).release().perform()

    # select_addon2 = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ScrollView")
    # select_addon2.click()

    # if course == 1 and product == 1:
    #     Addon_screen_fast_pay(driver)
    #     print("From Addon screen Fast pay selected")
    # else:
    #     save = driver.find_element_by_id("com.digicollect.pos:id/btn_addon_save")
    #     save.click()

    save = driver.find_element_by_id("com.digicollect.pos:id/btn_addon_save")
    save.click()

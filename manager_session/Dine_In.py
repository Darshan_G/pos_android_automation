from appium.webdriver.common.touch_action import TouchAction
# from session.Manage_Order import manage_order
from manager_session.CustomerSelectionScreen import Select_Customer
from manager_session.select_course_and_product import select_course_product


def dine_in_SingleTable(driver):
    # scroll = driver.find_element_by_id("com.digicollect.pos:id/recycler_view_table_list")
    # scroll.click()

    TouchAction(driver).press(x=770, y=1870).move_to(x=766, y=484).release().perform()

    TouchAction(driver).press(x=763, y=1870).move_to(x=756, y=456).release().perform()

    TouchAction(driver).press(x=770, y=1860).move_to(x=759, y=456).release().perform()

    TouchAction(driver).press(x=766, y=1839).move_to(x=770, y=522).release().perform()

    TouchAction(driver).press(x=766, y=1891).move_to(x=759, y=498).release().perform()

    TouchAction(driver).press(x=770, y=1881).move_to(x=763, y=498).release().perform()

    TouchAction(driver).press(x=770, y=1682).move_to(x=773, y=474).release().perform()

    number = 18
    select_table = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/"
                                                "android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
                                                ".FrameLayout/android.widget.RelativeLayout/androidx.drawerlayout"
                                                ".widget.DrawerLayout/android.widget.RelativeLayout/android.widget."
                                                "FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager"
                                                "/android.widget.RelativeLayout/android.view.ViewGroup/androidx."
                                                "recyclerview.widget.RecyclerView/android.widget"
                                                ".FrameLayout[" + str(number) + "]"
                                                                                "/android.widget.LinearLayout/android.widget.TextView[1]")
    select_table.click()
    next_btn = driver.find_element_by_id("com.digicollect.pos:id/btn_next")
    next_btn.click()

    # Proceed with Selecting the Customer #
    Select_Customer(driver)

    # Select Course Product #

    # select_course_product(driver)

    # Select Product Page # Selecting Category

    # select_cat = driver.find_element_by_xpath(
    #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
    #     ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android"
    #     ".widget.RelativeLayout/android.support.v4.widget.DrawerLayout/android.widget"
    #     ".RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android"
    #     ".widget.ScrollView/android.widget.RelativeLayout/android.support.v7.widget"
    #     ".RecyclerView[1]/android.widget.RelativeLayout[1]/android.widget.FrameLayout")
    # select_cat.click()
    #
    # # Selecting SubCategory
    #
    # sub_category = driver.find_element_by_xpath(
    #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
    #     ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android"
    #     ".widget.RelativeLayout/android.support.v4.widget.DrawerLayout/android.widget"
    #     ".RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android"
    #     ".widget.ScrollView/android.widget.RelativeLayout/android.support.v7.widget"
    #     ".RecyclerView[2]/android.widget.RelativeLayout["
    #     "5]/android.widget.FrameLayout/android.widget.ImageView")
    # sub_category.click()
    #
    # # Selecting Product Type
    #
    # product_type = driver.find_element_by_xpath(
    #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
    #     ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android"
    #     ".widget.RelativeLayout/android.support.v4.widget.DrawerLayout/android.widget"
    #     ".RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android"
    #     ".widget.ScrollView/android.widget.RelativeLayout/android.support.v7.widget"
    #     ".RecyclerView[3]/android.widget.RelativeLayout[3]/android.widget.FrameLayout")
    # product_type.click()
    #
    # # Selecting Product
    #
    # select_product = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout"
    #                                               "/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
    #                                               ".FrameLayout/android.widget.RelativeLayout/android.support.v4.widget"
    #                                               ".DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout"
    #                                               "/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView[4]/android.widget.FrameLayout[3]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.EditText")
    # select_product.click()
    #
    # # Addon screen
    #
    # # Selecting addon
    #
    # selecting_addon = driver.find_element_by_xpath(
    #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
    #     ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android"
    #     ".widget.RelativeLayout/android.support.v4.widget.DrawerLayout/android.widget"
    #     ".RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android"
    #     ".widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout"
    #     "/android.widget.ScrollView/android.widget.LinearLayout/android.widget"
    #     ".LinearLayout/android.widget.RelativeLayout["
    #     "2]/android.view.ViewGroup/android.widget.RelativeLayout["
    #     "2]/android.widget.FrameLayout")
    #
    # selecting_addon.click()
    #
    # # increase the quantity by one
    #
    # increase_quantity = driver.find_element_by_id("com.digicollect.pos:id/img_btn_addons_add")
    # increase_quantity.click()
    #
    # # Decrease the quantity by one
    #
    # decrease_quantity = driver.find_element_by_id("com.digicollect.pos:id/img_btn_addons_minus")
    # decrease_quantity.click()
    #
    # # Save to Cart
    #
    # driver.find_element_by_id("com.digicollect.pos:id/btn_addon_save").click()
    #
    # # Check Out
    #
    # Check_out = driver.find_element_by_id("com.digicollect.pos:id/cart_view_btn")
    # Check_out.click()
    #
    # # For scroll
    #
    # scrollable = driver.find_element_by_id("com.digicollect.pos:id/pager")
    # scrollable.click()
    #
    # TouchAction(driver).press(x=640, y=2233).move_to(x=1135, y=1068).release().perform()
    #
    # # Alert message handle
    #
    # place_order = driver.find_element_by_id("com.digicollect.pos:id/btn_place_order")
    # place_order.click()
    # alert_confirm_order = driver.find_element_by_id("android:id/button1")
    # alert_confirm_order.click()
    # driver.find_element_by_id("android:id/button2").click()
